# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# User specific aliases and functions

# Enable VIM Mode
	set -o vi

# Aliases
	alias x='exit'
	alias brc='vi ~/.bashrc'
	alias zrc='vi ~/.zshrc'
	alias ytdl='youtube-dl'
	alias vi=vim
	alias iza='cd ~/.izaxone'
	alias i3c='vi ~/.config/i3/config'

# Activate the fuzzy finder
# https://github.com/junegunn/fzf
	[ -f ~/.fzf.bash ] && source ~/.fzf.bash

# Display a random command every launch
	printf "Random command: \n";echo $(whatis -l $(ls /bin) 2>/dev/null | shuf -n 1)

