""" VIM PLUGINS using Plug https://github.com/junegunn/vim-plug
call plug#begin('~/.vim/plugged')
" VIM Airline https://github.com/vim-airline/vim-airline
Plug 'vim-airline/vim-airline'
" palenight.vim https://github.com/drewtempelmeyer/palenight.vim
Plug 'drewtempelmeyer/palenight.vim'
call plug#end()

set background=dark
colorscheme palenight


function! s:swap_lines(n1, n2)
    let line1 = getline(a:n1)
    let line2 = getline(a:n2)
    call setline(a:n1, line2)
    call setline(a:n2, line1)
endfunction

function! s:swap_up()
    let n = line('.')
    if n == 1
        return
    endif

    call s:swap_lines(n, n - 1)
    exec n - 1
endfunction

function! s:swap_down()
    let n = line('.')
    if n == line('$')
        return
    endif

    call s:swap_lines(n, n + 1)
    exec n + 1
endfunction

noremap <silent> <c-s-up> :call <SID>swap_up()<CR>
noremap <silent> <c-s-down> :call <SID>swap_down()<CR>
