#!/bin/bash

# This script is for setting up symbolic links for dotfiles. 

# WARNING: It is not recommended to run on any other distribution other than the ones listed below: 
# - Fedora

### SETUP FUNCTIONS ###
# These functions are used by the main functions to perform checks. If you are simply looking for the functionality, check the MAIN FUNCTIONS section instead. 

get_dependencies() {
	# Git
	if ! type "git" > /dev/null; then
	  echo Installing Git...
	  sudo dnf -y install git
	fi
}

### MAIN FUNCTIONS ###

dotfiles_setup() {
	### INSTALL DEPENDENCIES
	get_dependencies
	

	# CLONE DOTFILES
		[ -d "$HOME/.izaxone/dotfiles" ] && mv "$HOME/.izaxone/dotfiles" "$HOME/.izaxone/dotfiles.old"
		git clone https://gitlab.com/izaxone/dotfiles.git $HOME/.izaxone/dotfiles

	# SHELLS
		[ -f "$HOME/.zshrc" ] && rm -i "$HOME/.zshrc";ln -s $HOME/.izaxone/dotfiles/.zshrc $HOME/.zshrc 
		[ -f "$HOME/.bashrc" ] && rm -i "$HOME/.bashrc";ln -s $HOME/.izaxone/dotfiles/.bashrc $HOME/.bashrc 

	# VIM
		[ -d "$HOME/.vim" ] && rm -ri "$HOME/.vim";ln -s $HOME/.izaxone/dotfiles/.vim $HOME/.vim 
		[ -f "$HOME/.vimrc" ] && rm -i "$HOME/.vimrc";ln -s $HOME/.izaxone/dotfiles/.vimrc $HOME/.vimrc

	# i3 Window Manager
		# Check if .config exists
		[ ! -d "$HOME/.config" ] && mkdir "$HOME/.config"
		# Create the link
		[ -d "$HOME/.config/i3" ] && rm -ri "$HOME/.vim";ln -s "$HOME/.izaxone/dotfiles/i3" "$HOME/.config/i3" 

	# NVIM	
	[ -d "$HOME/.config/nvim/" ] && rm -rf "$HOME/.config/nvim";ln -s $HOME/.izaxone/dotfiles/nvim $HOME/.config/nvim 
}

### RUN THE FUNCTIONS ###
dotfiles_setup
