""" PLUGINS
"" ENABLE VIM PLUGIN MANAGER
call plug#begin('~/.config/nvim/plugged')

"" APPEARANCE
" Pale Night theme https://github.com/drewtempelmeyer/palenight.vim
Plug 'drewtempelmeyer/palenight.vim'
Plug 'vim-airline/vim-airline'

""" CLEANUP
call plug#end()

""" CONFIGURATION
"" COLOR SCHEME
" Pale Night Theme
set background=dark
colorscheme palenight

"" ENABLE TERMINAL COLORS
if (has("termguicolors"))                                                               
  set termguicolors                                                                     
endif

